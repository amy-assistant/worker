from amy_worker import *
import datetime
from faker import Faker


fake = Faker()


model = parse(load('./src/weights.yaml'), Functions(db))


def test_content(c=''):
    return model.compute(Message({'platform': 'telegram',
                                  'user': '+4368181861359',
                                  'content': c,
                                  'datetime': fake.date_time().isoformat(),
                                  'out': fake.boolean(),
                                  'sender': 123534613,
                                  'channel': 373725748}))


print(test_content('Hello World !'))
