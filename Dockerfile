FROM python:alpine

ENV AMY_DB_HOST='159.89.24.207'
ENV AMY_DB='amy'
ENV AMY_Q_HOST='159.89.24.207'

RUN pip install pymongo kombu pyyaml

WORKDIR /app

COPY weights.yaml weights.yaml
COPY amy_worker amy_worker

CMD [ "python", "-m", "amy_worker" ]