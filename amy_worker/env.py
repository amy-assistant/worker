import os

AMY_DB_HOST = os.environ.get('AMY_DB_HOST', 'localhost')
AMY_DB = os.environ.get('AMY_DB', 'amy')

AMY_Q_HOST = os.environ.get('AMY_Q_HOST', 'localhost')
AMY_Q_IN = os.environ.get('AMY_Q_IN', 'messages_in')
AMY_Q_OUT = os.environ.get('AMY_Q_OUT', 'messages_out')
