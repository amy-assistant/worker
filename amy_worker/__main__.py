from .load import parse, load
from .enrich import Functions
from .message import Message
from .connection import db, rabbit
from .env import AMY_Q_IN, AMY_Q_OUT
from kombu import Queue
from kombu.mixins import ConsumerProducerMixin
import json

users = db.users

model = parse(load('./weights.yaml'), Functions(db))


class Worker(ConsumerProducerMixin):
    def __init__(self, connection):
        self.connection = connection

    def get_consumers(self, Consumer, channel):
        return [
            Consumer(Queue(AMY_Q_IN), callbacks=[
                     self.on_message], accept=['json']),
        ]

    def on_message(self, body, message):
        m = Message(body)
        m.setImportance(model.compute(m))
        print(f'Publishing {m}')
        self.producer.publish(m.data, AMY_Q_OUT)
        message.ack()


print('Running worker Now')
rabbit.connect()
Worker(rabbit).run()
