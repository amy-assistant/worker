from bson import ObjectId
import datetime
from .connection import db, rabbit
from .enrich import uc, um


class Message:
    def __init__(self, data):
        self.data = data

    @property
    def content(self):
        return self.data['content']

    @property
    def user(self):
        if not hasattr(self, '_user'):
            self._user = db.users.find_one(
                {f'platforms.{self.data["platform"]}.username': self.data['user']})
        return self._user

    @property
    def user_id(self):
        return str(self.user['_id'])

    @property
    def channel_id(self):
        return str(self.channel['_id'])

    @property
    def channel(self):
        if not hasattr(self, '_channel'):
            self._channel = uc(self.user_id).find_one(
                {f'platforms.{self.data["platform"]}': self.data['channel']})
        return self._channel

    @property
    def contact(self):
        if not hasattr(self, '_contact'):
            self._contact = None
        return self._contact

    def doc(self):
        return {
            **self.data,
            'user': ObjectId(self.user_id),
            'channel': ObjectId(self.channel['_id']),
            'datetime': datetime.datetime.fromisoformat(self.data['datetime'])
            # 'contact': ObjectId(self.contact['_id']),
        }

    @property
    def datetime(self):
        return datetime.datetime.fromisoformat(self.data['datetime'])

    def save(self):
        self._id = um(self.user_id).insert_one(
            self.doc()).inserted_id

    def setImportance(self, value):
        if not 0 < value < 1:
            raise ValueError('Number out of range 0-1')
        self.data['importance'] = value

    def publish(self):
        q.put({'user': self.user_id, 'id': str(self._id)})
