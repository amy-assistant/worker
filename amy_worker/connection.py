from .env import AMY_DB, AMY_DB_HOST, AMY_Q_HOST
from pymongo import MongoClient
from kombu import Connection, Exchange, Queue, Producer, Consumer

mongo = MongoClient(AMY_DB_HOST)
db = mongo[AMY_DB]

rabbit = Connection(AMY_Q_HOST)
