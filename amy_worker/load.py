import yaml
from .weights import Node


def load(path):
    return yaml.load(open(path))


def get_function(obj, key):
    if hasattr(obj, key):
        return getattr(obj, key)
    print(f'{key} is not defined')
    return lambda *args, **kwargs: .5


def parse(data, obj, key='get'):
    '''
    model = parse(load('./weights.yaml'))
    model.compute(message, db)
    '''
    if type(data) is not dict:

        return Node(data, function=get_function(obj, key))
    return Node(data.pop('weight'), children=map(lambda x: parse(x[1], obj, f'{key}_{x[0]}'), data.items()))
