class Node:
    def __init__(self, weight, value=None, function=None, children=None):
        self.weight = weight
        self.isBranch = False
        if value:
            self.fixedvalue = value
        elif function:
            self.function = function
        else:
            self.children = set(children) if children else set()
            self.isBranch = True

    def __repr__(self):
        return f"<Node({self.weight}) [{'|'.join(map(repr, self.children)) if hasattr(self, 'children') else ''}]>"

    def value(self, args=()):
        if hasattr(self, 'function'):
            val = self.function(*args)
        elif hasattr(self, 'fixedvalue'):
            val = self.fixedvalue
        else:
            val = .5

        if not 0 < val < 1:
            raise ValueError('Number out of range 0-1')
        return val

    def __iadd__(self, node):
        if self.isBranch:
            self.children.add(node)
        return self

    def compute(self, *args):
        if self.isBranch:
            return (sum(map(lambda x: x.compute(*args), self.children)) / sum(map(lambda x: x.weight, self.children))) * self.weight
        else:
            return self.value(args) * self.weight

    addChild = __iadd__
