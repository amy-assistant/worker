from bson import ObjectId
from .connection import db, rabbit
import datetime


def um(user_id, db=db): return db.messages[user_id]


def uc(user_id, db=db): return db.channels[user_id]


class Functions:
    dt_all = (datetime.datetime.now() -
              datetime.datetime.fromtimestamp(0)).days

    def __init__(self, db):
        self.db = db

    @staticmethod
    def pastdays(days):
        return {'datetime': {'$gt': datetime.datetime.now() - datetime.timedelta(days)}}

    def user_message_count(self, user_id, days):
        return um(user_id, self.db).count_documents({
            **self.pastdays(days)
        })

    def channel_participation(self, user_id, channel_id, days):
        return um(user_id, self.db).count_documents({
            **self.pastdays(days),
            'channel': ObjectId(channel_id),
            'out': True
        })/(self.channel_message_count(user_id, channel_id, days) or 1)

    def channel_message_count(self, user_id, channel_id, days):
        return um(user_id, self.db).count_documents({
            **self.pastdays(days),
            'channel': ObjectId(channel_id),
        })

    def channel_message_user_part(self, user_id, channel_id, days):
        return self.channel_message_count(user_id, channel_id, days) / (self.user_message_count(user_id, days) or 1)

    def channel_frequency(self, user_id, channel_id, days):
        return um(user_id, self.db).count_documents({
            **self.pastdays(days),
            'channel': ObjectId(channel_id),
        })

    def message_len(self, content):
        return len(content)

    def message_token_count(self, content, tokens):
        return sum(map(lambda t: content.count(t), tokens))

    def get_channel_participattion_all(self, message):
        return self.channel_participation(message.user_id, message.channel_id, self.dt_all) or .5

    def get_channel_participattion_1d(self, message):
        return self.channel_participation(message.user_id, message.channel_id, 1) or .5

    def get_channel_count_all(self, message):
        return self.channel_message_user_part(message.user_id, message.channel_id, self.dt_all) or .1

    def get_channel_count_1d(self, message):
        return self.channel_message_user_part(message.user_id, message.channel_id, 1) or .1

    def get_channel_frequency_1d(self, message):
        return None or .5

    def get_channel_frequency_7d(self, message):
        return None or .5

    def get_contact_count_all(self, message):
        return None or .5

    def get_contact_count_1d(self, message):
        return None or .5

    def get_contact_frequency_1d(self, message):
        return None or .5

    def get_contact_frequency_7d(self, message):
        return None or .5

    def get_message_tokenizer_mentions(self, message):
        return None or .5

    def get_message_tokenizer_sentenze_marks(self, message):
        return self.message_token_count(message.content, '!?') / self.message_len(message.content) or .00001

    def get_message_datetime(self, message):
        return max(.001, min(((message.datetime.hour - 12) % 12)/12, .999))

    def get_message_length(self, message):
        return min(1 / self.message_len(message.content), .999)


def stringify_oid(message):
    for f in message:
        if type(message[f]) is ObjectId:
            message[f] = {'$oid': str(message[f])}
    return message
